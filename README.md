

# LIS4368-A4 

## Jessica Motlow

### Assignment 4: Server Side Validation

####Model View Controller (MVC) is a framework that divides an application's implementation into three discrete component roles: models, views, and controllers.

[http://localhost:9999/a4/](http://localhost:9999/a4/)

*Screenshot of failed form validation:*

![Failed Form Validation](img/lis4368a4failedvalidation.png "Proof the validation worked")

*Screenshot of passed form validation:*

![Passed Form Validation](img/lis4368a4passedvalidation.png "Proof the validation worked")
